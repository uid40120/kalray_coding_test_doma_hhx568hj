CC=gcc
CFLAGS=-Wall


ODIR=.

LIBS=-lpng16


_DEPS = png.h
DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))

_OBJ = png_transform.o
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))


$(ODIR)/%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS) 

PNG_TRANSOFRM: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS) 

.PHONY: clean

clean:
	rm -f $(ODIR)/*.o *~ core $(INCDIR)/*~ 