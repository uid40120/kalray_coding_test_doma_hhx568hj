
Kalray coding test
------------------

For usage and parameters, see below.

For building: 
	$ make
For building and running unit test:
	$ make_and_test.sh

Note: if you run the unit test script (unit_test_script.sh), images in folder "imgs/" will be taken as input, and output images will be written to "/imgs_transformed". The folder "golden/" contains golden references the output images are checked against.

Requirements: libpng (>=1.6) and imagemagick

Have fun!



History
-------

v1.5 FINAL :
- improved processing speed of -rgb mode by processing all three color channels in the same for loop. Could be done also with default mode.

v1.4 :
- added Jenkins.txt: project is up and running on Jenkins server (see JenkinsConsoleOutput.txt)

v1.3 :
- added makefile
- added unit tests: unit_test_script.sh 
- added make_and_test.sh (triggers build and then unit_test_script.sh)

v1.2 : 
- added new transformation (RGB gain on each channel, option -rgb)
- added comparaison mode
- commented code

v1.1 : 
- fixed bug from V1.0: default transformation runing (R->0, G=B)

v1.0 : 
- original version from Kalray, bugy....


Short description:
------------------
Takes an input PNG file, does a transformation on color channels, and write output PNG file to disk.


 
Avialable transformations:
--------------------------
    <> default mode:
 
        value of red channel of input image is set to 0, and blue channel is overwritten by green channel
    
        usage:    -INPUT_PNG_FILE -OUTPUT_PNG_FILE
 
        exemple:  ./PNG_TRANSOFRM imgs/curious_cat.png imgs_transformed/curious_cat_fliped.png
 
    <> rgb mode:
 
        user can specify gains for each RGB channels
        
        usage:    -INPUT_PNG_FILE -OUTPUT_PNG_FILE -rgb R_GAIN G_GAIN B_GAIN
 
        exemple:  ./PNG_TRANSOFRM imgs/kalray.png imgs_transformed/kalray_gain.png -rgb 1.2 0.8 0.8
 
    NOTE: additionnal transformations can easily be added in the startup check
 
 
special feature:
----------------
    <> comparaison mode:
 
        Additionnaly, the fonction can be used to compare two PNG images. If comparaison fails, returns -1
            
        usage:    -PNG_FILE_A -PNG_FILE_B -comp
 
        exemple: ./PNG_TRANSOFRM goldens/manycore-img_Rx1.2_Gx0.8_Bx0.8.png imgs_transformed/manycore-img_gain.png -comp
            
 
