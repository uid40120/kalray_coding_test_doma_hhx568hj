/*
 * Copyright 2002-2010 Guillaume Cottenceau.
 *
 * This software may be freely redistributed under the terms
 * of the X11 license.
 *
 */

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <png.h>


void
abort_(const char *s, ...)
{
    va_list args;
    va_start(args, s);
    vfprintf(stderr, s, args);
    fprintf(stderr, "\n");
    va_end(args);
    abort();
}

int x, y;

int i=1;
char* input_file;
char* output_file;
char* mode = "default";

struct decoded_image {
    int         w, h;
    png_byte    color_type;
    png_byte    bit_depth;
    png_structp png_ptr;
    png_infop   info_ptr;
    png_infop   end_info;
    int         number_of_passes;
    png_bytep   *row_pointers;
};

void
read_png_file(char *file_name, struct decoded_image *img)
{
    char header[8];        // 8 is the maximum size that can be checked

    /* open file and test for it being a png */
    FILE *fp = fopen(file_name, "rb");
    if (!fp)
        abort_("[read_png_file] File %s could not be opened for reading", file_name);
    fread(header, 1, 8, fp);
    if (png_sig_cmp((png_const_bytep)header, 0, 8))
        abort_("[read_png_file] File %s is not recognized as a PNG file", file_name);


    /* initialize stuff */
    img->png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

    if (!img->png_ptr)
        abort_("[read_png_file] png_create_read_struct failed");

    img->info_ptr = png_create_info_struct(img->png_ptr);
    if (!img->info_ptr)
        abort_("[read_png_file] png_create_info_struct failed");

    if (setjmp(png_jmpbuf(img->png_ptr)))
        abort_("[read_png_file] Error during init_io");


    png_init_io(img->png_ptr, fp);
    png_set_sig_bytes(img->png_ptr, 8);

    png_read_info(img->png_ptr, img->info_ptr);

    img->w      = png_get_image_width(img->png_ptr, img->info_ptr);
    img->h     = png_get_image_height(img->png_ptr, img->info_ptr);
    img->color_type = png_get_color_type(img->png_ptr, img->info_ptr);
    img->bit_depth  = png_get_bit_depth(img->png_ptr, img->info_ptr);

    img->number_of_passes = png_set_interlace_handling(img->png_ptr);
    png_read_update_info(img->png_ptr, img->info_ptr);


    /* read file */
    if (setjmp(png_jmpbuf(img->png_ptr)))
        abort_("[read_png_file] Error during read_image");

    img->row_pointers = (png_bytep*) malloc(sizeof(png_bytep) * img->h);
    for (y = 0; y < img->h; y++)
        img->row_pointers[y] = (png_byte*) malloc(png_get_rowbytes(img->png_ptr, img->info_ptr));

    png_read_image(img->png_ptr, img->row_pointers);
    fclose(fp);
}


static void
write_png_file(char *file_name, struct decoded_image *img)
{
  /* create file */
  FILE *fp = fopen(file_name, "wb");
  if (!fp)
    abort_("[write_png_file] File %s could not be opened for writing", file_name);


  /* initialize stuff */
  img->png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

  if (!img->png_ptr)
    abort_("[write_png_file] png_create_write_struct failed");

  img->info_ptr = png_create_info_struct(img->png_ptr);
  if (!img->info_ptr)
    abort_("[write_png_file] png_create_info_struct failed");

  if (setjmp(png_jmpbuf(img->png_ptr)))
    abort_("[write_png_file] Error during init_io");

  png_init_io(img->png_ptr, fp);


    /* write header */
    if (setjmp(png_jmpbuf(img->png_ptr)))
        abort_("[write_png_file] Error during writing header");

    png_set_IHDR(img->png_ptr, img->info_ptr, img->w, img->h,
        img->bit_depth, img->color_type, PNG_INTERLACE_NONE,
        PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

    png_write_info(img->png_ptr, img->info_ptr);


    /* write bytes */
    if (setjmp(png_jmpbuf(img->png_ptr)))
        abort_("[write_png_file] Error during writing bytes");

    png_write_image(img->png_ptr, img->row_pointers);


    /* end write */
    if (setjmp(png_jmpbuf(img->png_ptr)))
        abort_("[write_png_file] Error during end of write");

    png_write_end(img->png_ptr, NULL);

    /* cleanup heap allocation */
    for (y = 0; y < img->h; y++)
        free(img->row_pointers[y]);
    free(img->row_pointers);
    png_destroy_write_struct(&img->png_ptr, &img->info_ptr);

    fclose(fp);
}


static int
process_file(struct decoded_image *img)
{
    /* sanity check */
    printf("Checking PNG format\n");
    if (png_get_color_type(img->png_ptr, img->info_ptr) != PNG_COLOR_TYPE_RGBA){
        printf("[process_file] color_type of input file must be PNG_COLOR_TYPE_RGBA (%d) (is %d)", PNG_COLOR_TYPE_RGBA, png_get_color_type(img->png_ptr, img->info_ptr));
        return 1;
    }
    
    /* set RED channel to 0, and green channel to value of blue channel */
    printf("\nStarting processing\n");
    for (x = 0; x < img->w; x++)
      {
      for (y = 0; y < img->h; y++)
        {
          png_byte *row = img->row_pointers[y];
          png_byte *ptr = &(row[x * 4]);
          /* set red value to 0 */
          ptr[0]  = 0;
        }
      }

    for (x = 0; x < img->w; x++) {
        for (y = 0; y < img->h; y++) {
            png_byte *row = img->row_pointers[y];
            png_byte *ptr = &(row[x * 4]);
            /* Then set green value to the blue one */
            ptr[1]  = ptr[2];
        }
    }
    printf("Processing done\n");

    
    png_destroy_read_struct(&img->png_ptr, &img->info_ptr, NULL);

    return 0;
}

int
compare_images(struct decoded_image *imgA, struct decoded_image *imgB)
{
    /* checks if two images are the same. Used for unit testing, checking output images against reference from "golden" folder.

     first, we run a quick check to be sure that images have same size, then we go over all pixels, and check that they have the same value. */
    
    /* if images have different sizes, no need to go further */
    if ((imgA->w != imgB->w) | (imgA->h != imgB->h)){  printf("\n[NOK]: images are NOT identical (different sizes).\n"); return -1; }

    /* over all pixels */
    for (x = 0; x < imgA->w; x++)
      {
      for (y = 0; y < imgA->h; y++)
        {
          png_byte *rowA = imgA->row_pointers[y];
          png_byte *ptrA = &(rowA[x * 4]);
            
          png_byte *rowB = imgB->row_pointers[y];
          png_byte *ptrB = &(rowB[x * 4]);
          /* if at least one pixel differs, image is not the same */
            if ((ptrA[0]  != ptrB[0]) | (ptrA[1]  != ptrB[1]) | (ptrA[2]  != ptrB[2])){   printf("\n[NOK]: images are NOT identical (pixel values).\n"); return -1;}
        }
      }
    printf("\n[OK]: images are identical.\n");
    return 0;
}

static
int process_file_rgb(struct decoded_image *img, float *param){
    
    /* get gains for each channel */
    float r_gain = param[0];
    float g_gain = param[1];
    float b_gain = param[2];
    
    /* print them on console */
    printf("\n(R-G-B) input params: (%f - %f - %f)", r_gain, g_gain, b_gain);
    
    /* sanity check  */
    printf("\nChecking PNG format\n");
    if (png_get_color_type(img->png_ptr, img->info_ptr) != PNG_COLOR_TYPE_RGBA){
        printf("[process_file] color_type of input file must be PNG_COLOR_TYPE_RGBA (%d) (is %d)", PNG_COLOR_TYPE_RGBA, png_get_color_type(img->png_ptr, img->info_ptr));
        return 1;
    }

    /* apply gains on each channels  */
    printf("\nStarting RGB gain processing\n");
    
    /* Three channels together  */
    for (x = 0; x < img->w; x++)
      {
      for (y = 0; y < img->h; y++)
        {
          png_byte *row = img->row_pointers[y];
          png_byte *ptr = &(row[x * 4]);
          /* apply gain on red channel */
          ptr[0]  = ptr[0] * r_gain;
          /* apply gain on blue channel */
          ptr[1]  =  ptr[1] * g_gain;
          /* apply gain on green channel */
          ptr[2]  = ptr[2] * b_gain;
        }
      }
    
    /* clean up */
    printf("Processing done\n");
    png_destroy_read_struct(&img->png_ptr, &img->info_ptr, NULL);

    return 0;
}

/*
 short description:
 ------------------
 Takes an input PNG file, does a transformation on color channels, and write output PNG file to disk.
 
 avialable transformations:
 --------------------------
    <> default mode:
 
        value of red channel of input image is set to 0, and blue channel is overwritten by green channel
    
        usage:    -INPUT_PNG_FILE -OUTPUT_PNG_FILE
 
        exemple:  ./PNG_TRANSOFRM imgs/curious_cat.png imgs_transformed/curious_cat_fliped.png
 
    <> rgb mode:
 
        user can specify gains for each RGB channels
        
        usage:    -INPUT_PNG_FILE -OUTPUT_PNG_FILE -rgb R_GAIN G_GAIN B_GAIN
 
        exemple:  ./PNG_TRANSOFRM imgs/kalray.png imgs_transformed/kalray_gain.png -rgb 1.2 0.8 0.8
 
    NOTE: additionnal transformations can easily be added in the startup check
 
 
 special feature:
 ----------------
    <> comparaison mode:
 
        Additionnaly, the fonction can be used to compare two PNG images.
            
        usage:    -PNG_FILE_A -PNG_FILE_B -comp
 
        exemple: ./PNG_TRANSOFRM goldens/manycore-img_Rx1.2_Gx0.8_Bx0.8.png imgs_transformed/manycore-img_gain.png -comp
            
 */

int main(int argc, char **argv){
        
    /* we need at least 2 arguments: INPUT_PNG_FILE and OUTPUT_PNG_FILE (PNG_FILE_A and PNG_FILE_B in case of comparaison mode). */
    if (argc < 3)
        abort_("Usage: program_name <file_in> <file_out> (opt) [-m rgb r_gain g_gain, b_gain]");
    
    /* allocate memory for struct of input image */
    struct decoded_image *img = malloc(sizeof(struct decoded_image));
  
    /* input and output file paths */
    input_file = argv[1];
    output_file = argv[2];
    printf("\ninput file: %s", input_file);
    printf("\noutput file: %s", output_file);
        
    /* parse input parameters (if provided)  */
    i = 3;
    if (argc >3){
        
        /* RGB mode  */
        if (strcmp(argv[i], "-rgb") == 0)
        {
            i++;
            int t = 3;
            float *param = malloc(t * sizeof(float));
            param[0] = atof(argv[i]);
            param[1] = atof(argv[i+1]);
            param[2] = atof(argv[i+2]);
                
            printf("\nmode: RGB gain \n");
            printf("\nreading file %s \n", input_file);
            read_png_file(input_file, img);
            process_file_rgb(img, param);
            printf("\nwriting to file %s \n", output_file);
            write_png_file(output_file, img);
        }
        
        /* comparaison mode (used in unit test)  */
        if (strcmp(argv[i], "-comp") == 0)
        {
            /* allocate memory for struct of second image */
            struct decoded_image *img_golden = malloc(sizeof(struct decoded_image));
            printf("\nmode: comparaison\n");
            printf("\nreading file A %s", input_file);
            read_png_file(input_file, img);
            printf("\nreading file B %s", output_file);
            read_png_file(output_file, img_golden);
            printf("\nchecking if A is identical to B...");
            /* run comparaison between 2 images  */
            compare_images(img, img_golden);
        }

        /* Insert custom transformations here  */

    }
    
    /* default mode  */
    else{
        printf("\nmode: default\n");
        printf("\nreading file %s \n", input_file);
        read_png_file(input_file, img);
        process_file(img);
        printf("\nwriting to file %s \n", output_file);
        write_png_file(output_file, img);
        }
    
    return 0;

    }

