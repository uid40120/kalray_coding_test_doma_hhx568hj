#!/bin/sh

echo "========================================================"
echo "   Tests for original transformation (R->0 and B->G)"
echo "========================================================"
echo ""

echo "========================================================"
echo "        UNIT TEST #001"
echo "========================================================"

echo "------- curious_cat.png --------------------------------"
echo "========================================================"
./PNG_TRANSOFRM imgs/curious_cat.png imgs_transformed/curious_cat_fliped.png
echo ""
echo "-> checking against reference (golden)..."
./PNG_TRANSOFRM goldens/curious_cat.png imgs_transformed/curious_cat_fliped.png -comp
echo "done"

echo "========================================================"
echo "        UNIT TEST #002"
echo "========================================================"

echo "------- kalray.png -------------------------------------"
echo "========================================================"
./PNG_TRANSOFRM imgs/kalray.png imgs_transformed/kalray_fliped.png
echo ""
echo "-> checking against reference (golden)..."
./PNG_TRANSOFRM goldens/kalray.png imgs_transformed/kalray_fliped.png -comp
echo "done"

echo "========================================================"
echo "        UNIT TEST #003"
echo "========================================================"

echo "------- manycore-img.png -------------------------------"
echo "========================================================"
./PNG_TRANSOFRM imgs/manycore-img.png imgs_transformed/manycore-img_fliped.png
echo ""
echo "-> checking against reference (golden)..."
./PNG_TRANSOFRM goldens/manycore-img.png imgs_transformed/manycore-img_fliped.png -comp
echo "done"

echo "========================================================"
echo "Tests for second transformation (RGB)=(1.2, 0.8, 0.8)"
echo "--------------------------------------------------------"
echo ""

echo "========================================================"
echo "         UNIT TEST #004"
echo "========================================================"

echo "------- kalray.png -------------------------------------"
echo "========================================================"
./PNG_TRANSOFRM imgs/kalray.png imgs_transformed/kalray_gain.png -rgb 1.2 0.8 0.8
echo ""
echo "-> checking against reference (golden)..."
./PNG_TRANSOFRM goldens/kalray_Rx1.2_Gx0.8_Bx0.8.png imgs_transformed/kalray_gain.png -comp
echo "done"

echo "========================================================"
echo "         UNIT TEST #005"
echo "========================================================"

echo "------- manycore-img.png -------------------------------"
echo "========================================================"
./PNG_TRANSOFRM imgs/manycore-img.png imgs_transformed/manycore-img_gain.png -rgb 1.2 0.8 0.8
echo ""
echo "-> checking against reference (golden)..."
./PNG_TRANSOFRM goldens/manycore-img_Rx1.2_Gx0.8_Bx0.8.png imgs_transformed/manycore-img_gain.png -comp


